# IT Process Automation 
## Project 
1. home IT process automation for home network

2. IT process automation in the server area for Linux based server infrastructure.

### Programs installation
- etckeeper
- fail2ban
- s-nail
- grafana

### Securing and hardening the system
- [ ] Set up an SSH PublicKey-auth - optionally with two-factor authentication
- [ ] Set up an SSH tunnel

### Regular updates
- [ ] Create an Ansible playbook that regularly checks for updates, sends them a notification to a dedicated email address. 
- [ ] Verify that the Ansible playbook is working properly.

### Regular backups
- [ ] Set up an Ansible playbook that makes regular backups of the deposited host and stores them via SSH and borgbackup to the Ansible host on a backup hard disk. 
- [ ] Verify that the Ansible playbook is working properly. 

