# ITA-Projekttag
## Projekt
[following]

## Milestones

# Preparation

- [ ]  Consider which precautions have to be taken at the cable Fritz!Box to make the OpenWrt router

- [ ]  

### Setup and configuration of networks including corresponding firewalls
- [X]  Set up a Git repository.

- [X]  Set up a (virtual) device (https://shop.heise.de/router-joy-it-qr750i) with OpenWrt.

- [ ]  Set a secure password (meeting complexity requirements) for the OpenWrt web interface.

### Proxy
- [ ] 

- [ ]  Create a new user with sudo permissions ([link to official OpenWrt documentation] (https://openwrt.org/docs/guide-user/additional-software/create-new-users)).

- [ ]  Create for SSH access 

- [ ]  Deposit a SSH key.

### Securing 
- [ ]  Disable root access via SSH
- [ ]  Set up the following networks and document this accordingly.
    - [ ]  Local area network (LAN)
    - [ ]  Wireless local area network (WLAN)
    - [ ]  SmartHome (SH)
    - [ ]  Guest network 
    - [ ]  Windows network
    - [ ]  Test network

- [ ]  Set up a landing page for the guest network.

- [ ]  Set up a generally rather restrictive firewall for each network.
    - [ ]  Port 80: HTTP
    - [ ]  Port 443: HTTPS
    - [ ]  Port 50000
- [ ]  Set up a VPN on the OpenWrt using Wireguard.

- [ ]  Optional: Set up a way to make VoIP calls via the OpenWrt.
- [ ]  Weisen Sie nach, dass sämtliche (optionalen) Meilensteine entsprechend eingestellt wurden und zeigen Sie die Dokumentation vor.

## Network circle

| Network identifier | Network name                                  | Network color | Net-ID      | Gateway       | Router       | Subnetmask    | Number of hosts | Annotation            
|:-------------------|-----------------------------------------------|:--------------|:------------|:--------------|:-------------|:--------------|:----------------|:-------------------------------
| 2.1                | Local Area Network (LAN)                      | GREEN         | 10.2.10.0/24 | 10.2.10.1/24 | 10.2.10.1/24 | 255.255.255.0 | 254             | For devices connected with Ethernet cable.
| 2.2                | Wireless Local Area Network (WLAN)            | BLUE          | 10.2.20.0/24 | 10.2.20.1/24 | 10.2.20.1/24 | 255.255.255.0 | 254             | For devices connected with wireless connection.
| 2.3                | SmartHome                                     | ORANGE        | 10.2.30.0/24 | 10.2.30.1/24 | 10.2.30.1/24 | 255.255.255.0 | 254             | For SmartHome devices connected with wireless connection.
| 2.4                | Guest Wireless Local Area Network (Gast-WLAN) | GRAY          | 10.2.40.0/24 | 10.2.40.1/24 | 10.2.40.1/24 | 255.255.255.0 | 254             | For guests' devices conntected with wireless connection. 
| 2.5                | Windows Network (WinN)                        | LILA          | 10.2.50.0/24 | 10.2.50.1/24 | 10.2.50.1/24 | 255.255.255.0 | 254             | For Windows devices connected with wireless connection.
| 2.6                | Test-Network (TN)                             |               | 10.2.60.0/24 | 10.2.60.1/24 | 10.2.60.1/24 | 255.255.255.0 | 254             | For Ethernet cable-connected devices for testing.