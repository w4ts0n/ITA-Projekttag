https://www.kuketz-blog.de/stubby-verschluesselte-dns-anfragen-openwrt-teil5/

https://www.kuketz-blog.de/keine-werbung-und-tracker-mit-adblock-addon-openwrt-teil4/

https://www.kuketz-blog.de/hardening-ssh-und-luci-webzugang-openwrt-teil3/

https://www.kuketz-blog.de/openwrt-update-benachrichtigung-fuer-packages/

https://www.kuketz-blog.de/google-und-facebook-ip-adressen-blockieren/ 



>    Paket-Filterung bzw. Kontrolle des ausgehenden (Client-)Datenverkehrs via Firewall-Regeln
    Blockieren von Werbung, Trackern und Co. auf DNS-Ebene via Adblock-Paket
    Blockieren der IP-Adressen von Datensammlern wie Google, Facebook und Co. auf Basis von ASN-Informationen
    Trennung zwischen Wireless- und physisch angeschlossenen Geräten
    »Bändigen« meiner Windows-10-VirtualBox-Maschine (notwendig für Steuer- und Buchhaltungssoftware)
    DNS-over-TLS via Stubby und DNSSEC via dnsmasq

Quelle: https://www.kuketz-blog.de/netzwerkaufbau-im-heimnetz-digitaler-schutzschild-teil5/